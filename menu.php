<?php
  define('ROOT', '/php/');
  function currentPage(){
    $uri = $_SERVER['REQUEST_URI'];
    $uriArray = explode('/',$uri);
    $page = end($uriArray);
    $pageArray = explode('?',$page);
    $currentPage = $pageArray[0];
    return $currentPage;
  }

  function active($fileName){
    return (currentPage() == $fileName)? "active" : "";
  }

?>
<div class="menu">
  <ul>
    <li class="<?=active('index.php');?>"><a href="<?=ROOT?>index.php">Home</a></li>
  </ul>
</div>
